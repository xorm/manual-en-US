## 9.Execute SQL command

When you want `insert`, `update` or `delete`, then use `Exec` as below

```Go
sql := "update userinfo set username=? where id=?"
res, err := engine.Exec(sql, "xiaolun", 1) 
```
