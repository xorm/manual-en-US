### 5.3.Get one record
Fetch a single object by user

```Go
var user = User{ID:27}
has, err := engine.Get(&user)
// or has, err := engine.ID(27).Get(&user)

var user = User{Name:"xlw"}
has, err := engine.Get(&user)
```
